# http-fs

[![Build](https://gitlab.com/Douman/http-fs/badges/master/pipeline.svg)](https://gitlab.com/Douman/http-fs/pipelines)
[![Crates.io](https://img.shields.io/crates/v/http-fs.svg)](https://crates.io/crates/http-fs)
[![Documentation](https://docs.rs/http-fs/badge.svg)](https://docs.rs/crate/http-fs/)

## Features

- `tokio` - Enables `tokio` runtime integration. Enables `rt`.
- `hyper` - Enables `hyper` integration. Enables `http1` and `server` features.
