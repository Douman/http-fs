//!Axum adaptor
//!
//!Implements `Handler` onto `StaticFiles`
//!
//!## Usage
//!
//!```rust,ignore
//!
//!use axum::{routing, Router};
//!
//!use http_fs::config::{self, StaticFileConfig, TokioWorker};
//!use http_fs::StaticFiles;
//!
//!use std::path::Path;
//!
//!#[derive(Clone)]
//!pub struct DirectoryConfig;
//!impl StaticFileConfig for DirectoryConfig {
//!    type FileService = config::DefaultConfig;
//!    type DirService = config::DefaultConfig;
//!
//!    fn handle_directory(&self, _path: &Path) -> bool {
//!        true
//!    }
//!}

//!async fn example() {
//!    let static_files = StaticFiles::new(TokioWorker, DirectoryConfig);
//!
//!    //idk how to make this shitty Handler to infer T
//!    let app = Router::new().route("/", routing::any::<_, (), _, _>(static_files));
//!    // run it with hyper on localhost:3000
//!    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
//!        .serve(app.into_make_service())
//!        .await
//!        .unwrap();
//!}
//!```

use axum::handler::Handler;
use axum::http::request::Request;
use axum::http::header::HeaderMap;
use axum::response::Response;

use crate::config::{StaticFileConfig, FileServeConfig, FsTaskSpawner};
use crate::{StaticFiles, Body};

use std::io;
use core::{task, future};
use core::pin::Pin;

impl<W: FsTaskSpawner, C: FileServeConfig> axum::body::HttpBody for Body<W, C> {
    type Data = bytes::Bytes;
    type Error = io::Error;

    #[inline(always)]
    fn poll_data(self: Pin<&mut Self>, ctx: &mut task::Context<'_>) -> task::Poll<Option<Result<Self::Data, Self::Error>>> {
        self.poll_next(ctx)
    }

    #[inline(always)]
    fn poll_trailers(self: Pin<&mut Self>, _: &mut task::Context<'_>) -> task::Poll<Result<Option<HeaderMap>, Self::Error>> {
        task::Poll::Ready(Ok(None))
    }

    #[inline(always)]
    fn is_end_stream(&self) -> bool {
        self.is_finished()
    }
}

impl<W: FsTaskSpawner + Send + Clone + 'static, C: StaticFileConfig + Send + Clone + 'static, T, S, B> Handler<T, S, B> for StaticFiles<W, C> where W::FileReadFut: Send + 'static {
    type Future = future::Ready<Response>;

    #[inline(always)]
    fn call(self, req: Request<B>, _: S) -> Self::Future {
        let response = self.serve_http(req.method(), req.uri(), req.headers());
        future::ready(response.map(|body| axum::body::boxed(body)))
    }
}
