//!HTTP libraries adaptors

#[cfg(feature = "hyper")]
pub mod hyper;
#[cfg(feature = "axum")]
pub mod axum;
